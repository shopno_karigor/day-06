<html>
    <head>
        <title>Form</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" >
    </head>

    <body>
        <div class="container">
            <div class="row">
                <h1 class="col-lg-6 col-lg-offset-3">Form</h1>
                <form class="col-lg-6 col-lg-offset-3" action="action.php" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" placeholder="English" name="english" class="form-control"/>
                        <i class=" glyphicon glyphicon-pencil form-control-feedback"></i>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" placeholder="Mathmatics" name="math" class="form-control"/>
                        <i class=" glyphicon glyphicon-pencil form-control-feedback"></i>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" placeholder="Chemestry" name="chemestry" class="form-control"/>
                        <i class=" glyphicon glyphicon-pencil form-control-feedback"></i>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" placeholder="Biology" name="biology" class="form-control"/>
                        <i class=" glyphicon glyphicon-pencil form-control-feedback"></i>
                    </div>

                    <div class="form-group">
                        <input class="btn btn-success" type="submit" value="Do"/>
                    </div>

                </form>
            </div>
        </div>

    </body>
</html>